<?php
/**
 * @file
 * demo_popular.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function demo_popular_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'popular';
  $context->description = 'Popular content';
  $context->tag = 'Demo';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-popular-popular' => array(
          'module' => 'views',
          'delta' => 'popular-popular',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Demo');
  t('Popular content');
  $export['popular'] = $context;

  return $export;
}

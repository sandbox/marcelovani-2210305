api = 2
core = 7.x

projects[ctools][subdir] = contrib
projects[ctools][version] = 1.3

projects[diff][subdir] = contrib
projects[diff][version] = 3.2

projects[entity][subdir] = contrib
projects[entity][version] = 1.x-dev
projects[entity][download][type] = git
projects[entity][download][url] = http://git.drupal.org/project/entity.git
projects[entity][download][revision] = 5731f741e3366889e95b5357f1f85b0acc51a9fe

projects[features][subdir] = contrib
projects[features][version] = 1.0

projects[features_override][subdir] = contrib
projects[features_override][version] = 2.0-rc1

projects[pathauto][subdir] = contrib
projects[pathauto][version] = 1.2

projects[strongarm][subdir] = contrib
projects[strongarm][version] = 2.0

projects[token][subdir] = contrib
projects[token][version] = 1.5

projects[views][subdir] = contrib
projects[views][version] = 3.7

projects[nodequeue][subdir] = contrib
projects[nodequeue][version] = 2.0-beta1

projects[entitycache][subdir] = contrib
projects[entitycache][version] = 1.2

projects[xmlsitemap][subdir] = contrib
projects[xmlsitemap][version] = 2.0-rc2

projects[wysiwyg][subdir] = contrib
projects[wysiwyg][version] = 2.2

projects[addthis][subdir] = contrib
projects[addthis][version] = 2.1-beta1

projects[google_analytics][subdir] = contrib
projects[google_analytics][version] = 1.4
projects[google_analytics][type] = module

projects[link][subdir] = contrib
projects[link][version] = 1.2

projects[libraries][subdir] = contrib
projects[libraries][version] = 1.0

projects[field_collection][subdir] = contrib
projects[field_collection][version] = 1.x-dev
projects[field_collection][download][type] = git
projects[field_collection][download][url] = http://git.drupal.org/project/field_collection.git
projects[field_collection][download][revision] = 4e0a52349a3f97b346622cda2e0e9ceb24787604

projects[field_collection_bulkupload][subdir] = contrib
projects[field_collection_bulkupload][version] = 1.0-alpha1

projects[plupload][subdir] = contrib
projects[plupload][version] = 1.4

projects[taxonomy_display][subdir] = contrib
projects[taxonomy_display][version] = 1.1

projects[nodeformcols][subdir] = contrib
projects[nodeformcols][version] = 1.0

projects[media][subdir] = contrib
projects[media][version] = 1.4

projects[context][subdir] = contrib
projects[context][version] = 3.1

projects[field_group][subdir] = contrib
projects[field_group][version] = 1.3

projects[context][subdir] = contrib
projects[context][version] = 3.1

projects[date][subdir] = contrib
projects[date][version] = 2.7

projects[feeds][subdir] = contrib
projects[feeds][version] = 2.0-alpha8

projects[module_filter][subdir] = contrib
projects[module_filter][version] = 2.0-alpha2

projects[admin_menu][subdir] = contrib
projects[admin_menu][version] = 3.0-rc4

projects[devel][subdir] = contrib
projects[devel][version] = 1.4

projects[tao] = 3.0-beta4
projects[rubik] = 4.0-beta8
projects[omega] = 3.1

projects[omega_tools][subdir] = contrib
projects[omega_tools][version] = 3.0-rc4
projects[omega_tools][type] = module

; jQuery Cycle (for views_slideshow_cycle)
libraries[jquery_cycle][download][type] = "get"
libraries[jquery_cycle][download][url] = "http://malsup.com/jquery/cycle/release/jquery.cycle.zip?v2.99"
libraries[jquery_cycle][directory_name] = "jquery.cycle"

libraries[ckeditor][download][type] = get
libraries[ckeditor][download][url] = http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.5/ckeditor_3.5.tar.gz

libraries[photoswipe][download][type] = get
libraries[photoswipe][download][url] = http://github.com/downloads/codecomputerlove/PhotoSwipe/code.photoswipe-3.0.5.zip

libraries[plupload][download][type] = get
libraries[plupload][download][url] = https://github.com/downloads/moxiecode/plupload/plupload_1_5_4.zip

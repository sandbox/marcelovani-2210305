#!/bin/bash
set +x

echo
echo "[Info] Bootstrap is cloning the repo"

git clone --recursive --branch master http://git.drupal.org/sandbox/marcelovani/2210305.git modules/demo

echo "[Info] Building make file"

drush make modules/demo/modules/drupal-org.make --no-core -y --working-copy --no-gitinfofile --contrib-destination=.

echo "[Info] Enabling modules"

drush en -y demo_core

#echo "[Info] Generating content"
#drush en -y devel_generate
#drush generate-content 20

echo
